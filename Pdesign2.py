import sys, numpy as np
from Bio import SeqIO
from Bio.SeqUtils import MeltingTemp as mt

probe_len=20

for record in SeqIO.parse(sys.argv[1], "fasta"):
    print (record.id)
    seq=record.seq
#for depleting total/mRNA
pseq=seq.reverse_complement()
#for depleting aRNA
#pseq=seq
#generates full-length probe sequences
probes=[pseq[x:x+probe_len] for x in range(0, len(seq)-probe_len-1)]

def GC(probes):
    allP=[]
    allTM=[]
    for x in range(0, len(probes)):
        y=probes[x].upper()
        nTM=mt.Tm_NN(y)#, nn_table=mt.R_DNA_NN1)
        PT=(x,nTM)
        allTM.append(nTM)
        allP.append(PT)
    median=np.median(allTM)
    print (median)
    #creates a tuple containing another tuple as the first index with probes index and melt temp, and with the median melt temp as the second index
    new=(allP, median)
    return new

def GC_f(probes_TM): 
    lower=probes_TM[1]-5
    upper=probes_TM[1]+5
    i=[]
    print ('median melt temperature:'+str(probes_TM[1]))
    for x in probes_TM[0]:
        if x[1]>=lower and x[1]<=upper:
            i.append(x[0])
            print (x)
    return i                

def get_probe(probesi, probes):
    y=0
    flag=0
    g_probe=[]
    for x in probesi:
        print (probes[x])
        print (x, y, x-y)
        if x-y>21: 
            g_probe.append(str(probes[x]))
            y=x
        elif x-y>=0 and flag==0:
            g_probe.append(str(probes[x]))
            flag=1
    return (g_probe)

#def blastFA(probesi, probes):
#    F=open('blast_test.fa', 'w')
#    for x in probesi:
#        print (x)
#        F.write('>'+str(x)+'\n')
#        F.write(str(probes[x].reverse_complement())+'\n')
#    F.close()

probes_TM=GC(probes)
probesi=GC_f(probes_TM)
g_probes=get_probe(probesi, probes)

print (str(len(g_probes))+' before U selection')


G=open(str(record.id)+'.pfa', 'w')

for x in range(0, len(g_probes)):
    if g_probes[x][-1].upper()=='T':
        G.write(str(record.id)+"_"+str(x)+'\t'+''+'\t')
        G.write(str(g_probes[x][:-1])+'\t'+'U'+'\n')
for x in range(0, len(g_probes)):
    if g_probes[x][-1].upper()!='T':
        G.write(str(record.id)+"_"+str(x)+'\t'+''+'\t')
        G.write(str(g_probes[x])+'\t'+'20'+'\n')


G.close()
    
